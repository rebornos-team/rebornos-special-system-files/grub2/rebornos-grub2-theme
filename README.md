# rebornos-grub2-theme

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

RebornOS grub2 theme

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/grub2/rebornos-grub2-theme.git
```

